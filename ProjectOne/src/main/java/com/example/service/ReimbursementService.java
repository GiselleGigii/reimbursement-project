package com.example.service;
import java.time.LocalDate;
import java.util.List;




import com.example.dao.ReimbursementDaoImpl;
import com.example.model.Reimbursement;


public class ReimbursementService {
	

	
		private ReimbursementDaoImpl rDao;
		
		public ReimbursementService() {
		}

		public ReimbursementService(ReimbursementDaoImpl rDao) {
			super();
			this.rDao = rDao;
		}
		
		
		public void insertReimb(double amount, LocalDate submitted, String description, int authorId, int typeId) {

			rDao.newReimb(amount, submitted, description, authorId, typeId);


			
		}
		
		
		
		public Integer convertTypeId(String type) {
			int typeId = 1;
			if(type.equals("travel")) {
				typeId = 1;
			}
			else if(type.equals("gas")) {
				typeId = 2;
			}
			else if(type.equals("food")) {
				typeId = 3;
			}
			return typeId;
		}
		
		public void alterReimb(int reimbId, LocalDate resolved, int resolver, int statusId) {
			rDao.adjustReimb(reimbId, resolved, resolver, statusId);
		}
		
		
		public List<Reimbursement> getUserReimbs(int authorId){
			List<Reimbursement>reimbList = rDao.allReimbForUser(authorId);
			return reimbList;
		}
		
		public List<Reimbursement> getPendingReimbs(int authorId){
			List<Reimbursement>reimbList = rDao.pendingReimbs(authorId);
			return reimbList;
		}
		
		public List<Reimbursement> getAllReimbs(){
			List<Reimbursement>reimbList = rDao.allReimbsMan();
			return reimbList;
		}
		
		public List<Reimbursement> getManPendingReimbs(){
			List<Reimbursement>reimbList = rDao.pendingReimbsMan();
			return reimbList;
		}
		
		public List<Reimbursement> getSumPerUser(){
			List<Reimbursement>amountList = rDao.amountPerUser();
			return amountList;
		}
		
		public List<Reimbursement> getSumPerType(){
			List<Reimbursement>typeList = rDao.amountPerType();
			return typeList;
		}
		
		

	}

	
	
	






