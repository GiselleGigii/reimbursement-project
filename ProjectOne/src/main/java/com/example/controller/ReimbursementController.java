    package com.example.controller;

import java.time.LocalDate;

import com.example.model.User;
import com.example.service.ReimbursementService;



import io.javalin.http.Handler;


public class ReimbursementController {
	
	
private ReimbursementService rServ = new ReimbursementService();


	
	
public ReimbursementController() {}
public ReimbursementController(ReimbursementService rServ) {
	super();
	this.rServ = rServ;
	
}
	
	
	


public final Handler NEWREIMB = (ctx) ->{
	System.out.println("In new reimb");
	
	User sessuser = (User)ctx.sessionAttribute("currentuser");
	System.out.println("Got User "+sessuser);
	
	
	int authorId = sessuser.getUserId();
	double amount = Double.parseDouble(ctx.formParam("amount"));
	int typeId = rServ.convertTypeId(ctx.formParam("typeId"));
	LocalDate submitdate = LocalDate.parse(ctx.formParam("submitdate"));
	String desc = ctx.formParam("description");
	rServ.insertReimb(amount, submitdate, desc, authorId, typeId);
	ctx.status(200);
	ctx.redirect("/html/emphome.html");
	
};


	
	public final Handler ALTERREIMB = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Inside Alterreimb");
		int reid = Integer.parseInt(ctx.formParam("reimbId"));
		LocalDate resolvedate = LocalDate.parse(ctx.formParam("resolvedate"));
		int resolver = sessuser.getUserId();
		int statusId = Integer.parseInt(ctx.formParam("statusId"));
		rServ.alterReimb(reid, resolvedate, resolver, statusId);
		ctx.status(200);
		ctx.redirect("/html/manhome.html");
	};
	
	
	public final Handler PASTREIMBUSER = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Got User in PASTREIMBUSER");
		int authId = sessuser.getUserId();
		ctx.status(200);
		ctx.json(rServ.getUserReimbs(authId));
	};
	
	public final Handler PENDINGREIMBUSER = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Got User in PENDINGREIMBUSER");
		int authId = sessuser.getUserId();
		ctx.status(200);
		ctx.json(rServ.getPendingReimbs(authId));
	};
	
	public final Handler ALLREIMBS = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getAllReimbs());
	};
	
	public final Handler PENDINGREIMBMAN = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getManPendingReimbs());
	};
	
	public final Handler AMOUNTLIST = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getSumPerUser());
	};
	
	public final Handler TYPELIST = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getSumPerType());
	};
	
	
	
	
	

}



	
	


	
	
		
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	






















	
	
	/*

	
		private static ReimbursementController reimbursementController;
		ReimbursementService reimbursementService;
		
		private ReimbursementController() {
			reimbursementService = new ReimbursementServiceImpl();
		}
		
		public static ReimbursementController getInstance() {
			if(reimbursementController == null)
				reimbursementController = new ReimbursementController();
			return reimbursementController; 
		}
		
		
		
		//Financial Manager 
		public void getReimbBasedOnFilter(HttpServletRequest req, HttpServletResponse resp) throws IOException{
			resp.setContentType("apllication/json");
			PrintWriter out = resp.getWriter();
			
			String reimbStatus = req.getParameter("Status");
			List<Reimbursement> reimbursementList; 
			
			if(reimbStatus.equals("Pending")) {
				reimbursementList = reimbursementService.viewRiembWithFilter(reimbStatus);
				out.println(new ObjectMapper().writeValueAsString(new Response("Pending reimbursement retrieved", true, reimbursementList)));
				
				
			}else if(reimbStatus.equals("Approved")){
				reimbursementList = reimbursementService.viewRiembWithFilter(reimbStatus);
				out.println(new ObjectMapper().writeValueAsString(new Response("Aproved reimbursement retrieved", true, reimbursementList)));
				
			}else if(reimbStatus.equals("Denied")) {
				reimbursementList = reimbursementService.viewRiembWithFilter(reimbStatus);
				out.println(new ObjectMapper().writeValueAsString(new Response("Denied reimbursement retrieved", true, reimbursementList)));
				
			}else {
				reimbursementList = reimbursementService.getAllReimbursement();
				out.println(new ObjectMapper().writeValueAsString(new Response("All reimbursement retrieved", true, reimbursementList)));
			}
			
			for(Reimbursement reimbursement : reimbursementList) {
				System.out.println(reimbursement);
			}
		}
		
		// Financial Manager 
		
		public void resolveReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException{
			resp.setContentType("application/json");
			PrintWriter out = resp.getWriter();
			
			String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			Reimbursement reimbursement = new ObjectMapper().readValue(requestBody, Reimbursement.class);
			reimbursementService.resolveReimbursement(reimbursement);
			out.println(new ObjectMapper().writeValueAsString(new Response("Reimbursements status updated", true, null)));
		}
		
		
		
		//Employee
		public void getEmployeeReimb(HttpServletRequest req, HttpServletResponse resp) throws IOException{
			resp.setContentType("application/json");
			PrintWriter out = resp.getWriter();
			Integer usersId = Integer.parseInt(req.getParameter("usersId"));
			out.println(new ObjectMapper().writeValueAsString(new Response("Reimbursement retrieved", true, reimbursementService.getEmployeeReimbursement(usersId))));
			
			for(Reimbursement reimbursement : reimbursementService.getEmployeeReimbursement(usersId)) {
				System.out.println(reimbursement);
			}
		}
		
		
		//Employee
		public void createReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException{
			resp.setContentType("application/json");
			PrintWriter out = resp.getWriter();
			
			String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			
			Reimbursement reimbursement = new ObjectMapper().readValue(requestBody, Reimbursement.class);
			reimbursementService.submitNewEmployeeReimbursement(reimbursement);
			System.out.println(reimbursement);
			
			out.println(new ObjectMapper().writeValueAsString(new Response("Reimbursement created", true, null)));
			out.println(reimbursement);
		}
		
		
	
*/
	

