package com.example.dao;


import java.time.LocalDate;
import java.util.List;


import com.example.model.Reimbursement;


public interface ReimbursementDao {
	void newReimb(double amount, LocalDate submitted, String description, int authorId, int typeId);
	void adjustReimb(int reimbId, LocalDate resolved, int resolverId, int statusId);
	List<Reimbursement> allReimbForUser(int userId);
	List<Reimbursement> pendingReimbs(int userId);
	List<Reimbursement> pendingReimbsMan();
	List<Reimbursement> allReimbsMan();
	List<Reimbursement> amountPerUser();
	List<Reimbursement> amountPerType();

	
	


	
	

	
	

}
