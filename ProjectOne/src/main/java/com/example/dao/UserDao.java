package com.example.dao;


import com.example.model.User;

public interface UserDao {
	String getRole(int roleId);
	User getUser(String username);
	
}
