
Feature: Manager Logging in to ERS App
	As a Manager, I wish to login to the ERS App using proper Credentials
  I want to use this template for my feature file

  
  Scenario: Succesful Login to the ERS App 
    Given a user is at the login page
    And a user is at the login page
    When a user inputs their user name
    And a user inputs their password
    And a user submits the information
    Then that user is a user of role Manager
    And the user is redirected to the Manager ERS Page 
    
    Scenario: Failing into Login to ERS App
		Given a user is at the login page
	  When a user incorrectly inputs their user name 
		And a user incorrectly inputs their password 
		But a user submits the information 
		Then the user should see the span of Enter your userName and password correct 